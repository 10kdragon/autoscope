var localVideo;
var localStream;
var lv;
var rv;
var rv_cam;
var serverConnection;
var uuid;
var peerConnection;
var clientPeerConnection;
var peerConnectionConfig = {'iceServers': [{'url': 'stun:stun.services.mozilla.com'}, {'url': 'stun:stun.l.google.com:19302'}]};
navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;

var getChromeVersion = function() {
    var raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);
    return parseInt(raw[2]);
};

var webrtc = new SimpleWebRTC({
  localVideoEl: 'localVideo',
  remoteVideosEl: '' // empty string
});

var button = document.getElementById('btnStartShare'),
    setButton = function (bool) {
        button.value = bool ? 'share screen' : 'stop sharing';
    };

setButton(true);

if (!webrtc.capabilities.supportScreenSharing) {
    button.disabled = 'disabled';
    button.value = 'not supported'
}
webrtc.on('localScreenRemoved', function () {
    setButton(true);
});


$('#btnStartShare').click(function () {
    var chromeVerion = getChromeVersion();
    if (webrtc.getLocalScreen()) {
        if (chromeVerion >= 47) {
            webrtc.getLocalScreen().getTracks()[0].stop();
        } else {
            webrtc.stopScreenShare();
        }
        setButton(true);
    } else {
        webrtc.shareScreen(function (err) {
            if (err) {
                setButton(true);
            } else {
                setButton(false);
            }
        });
    }
});

$('#btnCameraShare').click(function () {
   var constraints = {
        video: true,
        audio: false,
    };

    if(navigator.getUserMedia) {
        navigator.getUserMedia(constraints, getUserMediaSuccess, errorHandler);
    } else {
        alert('Your browser does not support getUserMedia API');
    }
 
});

$('#btnStartShare1').click(function() {
    if (navigator.getUserMedia) {
        var screen_constraints = {
            mandatory: {
                chromeMediaSource: 'screen',
                maxWidth: 1920,
                maxHeight: 1080,
                minAspectRatio: 1.77
            },
            optional: []
        };
        navigator.getUserMedia({ video: screen_constraints, audio: false, }, onSuccess, onFail);
    }
    else {
        alert('webRTC not available');
    }
});

function onSuccess(stream) {
    localStream = stream;
}

function onFail() {
    alert("fail to get user media!");
}

// local screen obtained
webrtc.on('localScreenAdded', function (video) {
    video.onclick = function () {
        video.style.width = video.videoWidth + 'px';
        video.style.height = video.videoHeight + 'px';
    };
    $('#vid_src').html(webkitURL.createObjectURL(webrtc.getLocalScreen()));
    localVideo = video;
    document.getElementById('localScreenContainer').appendChild(video);
    $('#localScreenContainer').show();

    //send my uuid to server for broadcasting
    serverConnection.send(JSON.stringify({'type': 'new_uuid','data': uuid}));
    console.log("send UUID!!");

    localStream = webrtc.getLocalScreen(); 
    //peerConnection.addStream(localStream);
    //peerConnection.createOffer(gotDescription, errorHandler);

});
// local screen removed
webrtc.on('localScreenStopped', function (video) {
    document.getElementById('localScreenContainer').removeChild(localVideo);
    $('#localScreenContainer').hide();
    //reset the button state
    setButton(true);
});


//============= set up p2p =====================


function pageReady() {
    uuid = guid();

    lv= document.getElementById('lv');
    rv= document.getElementById('rv');

    serverConnection = new WebSocket('wss://10.148.80.35:8090/sock');
    serverConnection.onmessage = gotMessageFromServer;

    var constraints = {
        video: true,
        audio: false,
    };

    //if(navigator.getUserMedia) {
    //    navigator.getUserMedia(constraints, getUserMediaSuccess, errorHandler);
    //} else {
    //    alert('Your browser does not support getUserMedia API');
    //}
    
    return;

    peerConnection = new RTCPeerConnection(peerConnectionConfig);
    peerConnection.onicecandidate = gotIceCandidate;
    peerConnection.onaddstream = gotRemoteStream;
}

function getUserMediaSuccess(stream) {
    //send my uuid to server for broadcasting
    serverConnection.send(JSON.stringify({'type': 'new_uuid','data': uuid}));
    console.log("send UUID!!");

    localStream = stream;
    lv.src = window.URL.createObjectURL(stream);
}

function errorHandler(error) {
    console.log(error);
}

function Responder() {
    this.connection = new RTCPeerConnection(peerConnectionConfig);
    
    //this.onIceCandidate = function(event) {
    //    if(event.candidate != null) {
    //                    serverConnection.send(JSON.stringify({'ice': event.candidate, 'to': _to, 'from':_from, 'from_presenter':'1'}));
    //    }
    //};
    //this.onDescription = function(description) {
    //        c.setLocalDescription(description, function () {
    //            serverConnection.send(JSON.stringify({'sdp': description, 'to': this.to, 'from':this.from, 'from_presenter':'1'}));
    //        }, function() {console.log('set description error')});
    //};
    this.from = "";
    this.to = "";
}

function makeOnIceCandidate(from, to, from_presenter) {
    return function(event) {
        if(event.candidate != null) {
                        serverConnection.send(JSON.stringify({'ice': event.candidate, 'to': to, 'from':from, 'from_presenter': from_presenter}));
        }
    };
}

function makeOnDescription(from, to, from_presenter, conn) {
    return function(description) {
            conn.setLocalDescription(description, function () {
                serverConnection.send(JSON.stringify({'sdp': description, 'to': to, 'from':from, 'from_presenter':from_presenter}));
            }, function() {console.log('set description error')});
    };
}

function Receiver() {
    //this.onIceCandidate = function(event) {
    //    if(event.candidate != null) {
    //        serverConnection.send(JSON.stringify({'ice': event.candidate, 'to': this.to, 'from':this.from}));
    //    }
    //};
    //this.onDescription = function(description) {
    //    clientPeerConnection.setLocalDescription(description, function () {
    //        serverConnection.send(JSON.stringify({'sdp': description, 'to': this.to, 'from':this.from}));
    //    }, function() {console.log('set description error')});
    //};
    this.from = "";
    this.to = "";
}

var responderList = [];

function gotMessageFromServer(message) {
    //debug
    console.log("From server: " + message.data);

    if(!peerConnection) start(false);

    var signal = JSON.parse(message.data);
    if(signal.sdp) {
        if (signal.to == uuid) {
            //if this desc is from Presenter
            if (signal.from_presenter === true) {
                var receiver = new Receiver();
                receiver.from = uuid;
                receiver.to = signal.from;

                clientPeerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp), function() {
                    clientPeerConnection.createAnswer(makeOnDescription(receiver.from, receiver.to, false, clientPeerConnection), errorHandler);
                }, errorHandler);

                return;
            }
            for (i = 0; i < responderList.length; i += 1) {
                if (signal.from == responderList[i].to) {
                    responderList[i].connection.setRemoteDescription
                    (new RTCSessionDescription(signal.sdp),function() {}, errorHandler);
                }
            }
        }

        //peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp), function() {
        //    peerConnection.createAnswer(gotDescription, errorHandler);
        //}, errorHandler);
    } else if(signal.ice) {
        if (signal.to == uuid) {
            //if this ice is from Presenter
            if (signal.from_presenter === true) {
                clientPeerConnection.addIceCandidate(new RTCIceCandidate(signal.ice));
                return;
            }
            for (i = 0; i < responderList.length; i += 1) {
                if (signal.from == responderList[i].to) {
                    responderList[i].connection.addIceCandidate(new RTCIceCandidate(signal.ice));
                }
            }
        }
    } else if(signal.uuid_list) {
       $('#activeVideoList').empty();
       var uuidArray = signal.uuid_list.split(',');
       var text = "";
       for (i=0; i<uuidArray.length; i++) {
            text += ('<section class="4u" onclick=onVideoSelected("' + uuidArray[i] + '")><div>' + uuidArray[i] + '</div>' + '<img src="images/pic05.jpg" alt="Bcast Me!" height="200"></section>');
            //text += ('<li onclick=onVideoSelected("' + uuidArray[i] + '")>' + uuidArray[i] + '</li>');  
       }
       $('#activeVideoList').append(text);
    } else if(signal.type) {
        if (signal.type == 'request_ice_and_desc') {
            if (signal.to == uuid) {
                //Presenter create conn and send ice & desc
                //var conn = new RTCPeerConnection(peerConnectionConfig);
                var responder = new Responder();
                //responder.connection = conn;
                responder.to = signal.from;
                responder.from = uuid;
                var conn = responder.connection;
                conn.onicecandidate = makeOnIceCandidate(responder.from, responder.to, true);
                conn.addStream(localStream);
                conn.createOffer(makeOnDescription(responder.from, responder.to, true, conn));
                responderList.push(responder);
            }
            
        }
    }
}

function onVideoSelected(selectedId)
{
    selectedId = selectedId.trim();

    //creat my local peer as audience
    clientPeerConnection = new RTCPeerConnection(peerConnectionConfig);
    var receiver = new Receiver();
    receiver.to = selectedId;
    receiver.from = uuid;
    clientPeerConnection.onicecandidate = makeOnIceCandidate(receiver.from, receiver.to, false, clientPeerConnection);

    clientPeerConnection.onaddstream = gotRemoteStream;

    serverConnection.send(JSON.stringify(
        {'from':uuid, 'to':selectedId, 'type':'request_ice_and_desc','data':''}
    ));
}

function start(isCaller) {
    
}

function gotDescription(description) {
    console.log('got description');
    peerConnection.setLocalDescription(description, function () {
        serverConnection.send(JSON.stringify({'sdp': description}));
    }, function() {console.log('set description error')});
}

function gotIceCandidate(event) {
    if(event.candidate != null) {
        serverConnection.send(JSON.stringify({'ice': event.candidate}));
    }
}


function gotRemoteStream(event) {
    console.log('got remote stream');
    remoteVideo.src = window.URL.createObjectURL(event.stream);
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

pageReady();
