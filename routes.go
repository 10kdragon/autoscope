package main

import (
	"github.com/gorilla/mux"
	"net/http"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type StaticServe struct {
	PathPref   string
	FolderPath string
}

type Routes []Route
type StaticServes []StaticServe

func NewRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	for _, serve := range staticFiles {
		router.
			PathPrefix(serve.PathPref).
			Handler(http.FileServer(http.Dir(serve.FolderPath)))
	}

	return router
}

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		IndexHandler,
	},
	Route{
		"TestSocket",
		"GET",
		"/test_sock",
		TestSocketHandler,
	},
}

var staticFiles = StaticServes{
	StaticServe{
		"/",
		"./static/",
	},
	StaticServe{
		"/js",
		"./static/js/",
	},
}
