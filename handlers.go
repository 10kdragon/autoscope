package main

import (
	"encoding/json"
	"golang.org/x/net/websocket"
	"net/http"
	"strings"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	data := struct {
		WebsocketURL string
	}{
		WebsocketURL: "wss://10.148.80.35:8090/sock",
	}

	templateMap["index"].Execute(w, data)
}

func TestSocketHandler(w http.ResponseWriter, r *http.Request) {
	data := struct {
		WebsocketURL string
	}{
		WebsocketURL: "wss://localhost:8090/sock",
	}

	templateMap["ws"].Execute(w, data)

}

//websocket handler
func SocketHandler(ws *websocket.Conn) {
	//io.Copy(ws, ws)

	clientSockets = append(clientSockets, ws)
	websocket.Message.Send(ws, "{\"uuid_list\": \""+strings.Join(activePresenterList, ",")+"\"}")

	for {
		var message string
		if err := websocket.Message.Receive(ws, &message); err != nil {
			println("Error receiving message from client!")
			break
		}
		println("Receiving:" + message)

		//sendout := "Received: " + message
		//if err := websocket.Message.Send(ws, sendout); err != nil {
		//	println("Error sending message to client!")
		//	break
		//}

		if strings.Contains(message, "new_uuid") {
			var msg Message
			err := json.Unmarshal([]byte(message), &msg)
			if err != nil {
				println("Error paring JSON!")
				broadcast(clientSockets, message)
				continue
			} else {
				println("Success paring JSON.")
				if msg.Type == "new_uuid" {
					activePresenterList = append(activePresenterList, msg.Data)
					broadcast(clientSockets, "{\"uuid_list\": \""+strings.Join(activePresenterList, ",")+"\"}")
					continue
				}
			}
		}

		broadcast(clientSockets, message)
	}
}

func broadcast(clients []*websocket.Conn, msg string) {
	for _, c := range clients {
		if err := websocket.Message.Send(c, msg); err != nil {
			//if i == len(clients)-1 {
			//	clients = clients[:i]
			//} else {
			//	println("###" + strconv.Itoa(i))
			//	println(len(clients))
			//	clients = append(clients[:i], clients[i+1:]...)
			//}
		}
	}
}
